package util.responsews;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import util.message.Error;

public class ResponseBuilder {
	public Response getResponseOK(String responseString) {
		return Response.status(Status.OK).type(MediaType.APPLICATION_JSON).entity(responseString).build();
	}
	
	public Response getResponseNotFoud(String responseString) {
		return Response.status(Status.NOT_FOUND).type(MediaType.APPLICATION_JSON).entity(responseString).build();
	}
	
	public Response getResponseUnauthorized(String responseString) {
		return Response.status(Status.UNAUTHORIZED).type(MediaType.APPLICATION_JSON).entity(responseString).build();
	}
	
	public Response getResponseConflict(String responseString) {
		return Response.status(Status.CONFLICT).type(MediaType.APPLICATION_JSON).entity(responseString).build();
	}
	
	public Response getDefaultResponseError() {
		Error error = new Error();
		error.setCodigo(0);
		error.setMensagem("Servišo indisponÝvel");
		return Response.status(Status.INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_JSON).entity(new Gson().toJson(error)).build();
	}
}
