package util.message;

import java.io.Serializable;

public class Error implements Serializable{
	
	public static final int DEFAULT_ERROR_SERVER = 1;
	public static final int ERROR_EMAIL_DUPLICATE = 2;
	public static final int ERROR_USER_AUTH = 3;
	
	public static final String MESSAGE_DEFAULT_ERROR_SERVER = "Servi�o indispon�vel";
	public static final String MESSAGE_ERROR_EMAIL_DUPLICATE = "E-mail j� cadastrado";
	public static final String MESSAGE_ERROR_USER_AUTH = "Falha na autentica��o do usu�rio";
	
	private int codigo;
	private String mensagem;
		
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public static Error buildMessage(int codigo, String mensagem) {
		Error error = new Error();
		error.setCodigo(codigo);
		error.setMensagem(mensagem);
		return error;
	}
	
}
