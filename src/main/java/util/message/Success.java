package util.message;

import java.io.Serializable;

public class Success implements Serializable {
	public static final String MESSAGE_SUCCESS_NEW_USER = "Usu�rio cadastrado com sucesso";
	public static final String MESSAGE_SUCCESS_USER_AUTH = "Usu�rio Autenticado";
	public static final String MESSAGE_SUCCESS_UPDATE_PERFIL = "Perfil alterado";
	private String mensagem;
	
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	public Success() {
	}

	public static Success buildMessage(String mensagem) {
		Success success = new Success();
		success.setMensagem(mensagem);
		return success;
	}
}
