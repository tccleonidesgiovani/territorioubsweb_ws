package dao;

import java.sql.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import bean.Opcao;
import bean.Pergunta;
import bean.Questionario;

public class QuestionarioDao {

	public static List<Questionario> listaQuestionariosFull() throws ClassNotFoundException, SQLException {
		String sql = "select t1.id, t1.descricao, t1.ordem, t1.aplicacao, t1.ultima_alteracao, t1.id_opcao_dependente, "
				+ "t2.id_opcao_dependente, t2.id, t2.descricao, t2.ordem, t2.tipo, "
				+ "t3.id, t3.descricao, t3.ordem from questionario as t1 "
				+ "join pergunta as t2 on t2.id_questionario = t1.id "
				+ "left join opcao as t3 on t3.id_pergunta = t2.id order by t1.ordem, t2.ordem, t3.ordem;";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstm = conn.prepareStatement(sql);
		ResultSet rs = pstm.executeQuery();
		List<Questionario> questionarios = new ArrayList<Questionario>();
		int idQuestionario = -1;
		int idPergunta = -1;
		Questionario questionario;
		List<Pergunta> perguntas = null;
		Pergunta pergunta = null;
		List<Opcao> opcoesDependentes = new ArrayList<Opcao>();
		List<Pergunta> perguntasDependentes = new ArrayList<Pergunta>();
		List<Questionario> questionariosDependentes = new ArrayList<Questionario>();
		List<Opcao> opcoes = null;
		while (rs.next()) {
			if (idQuestionario != rs.getInt("t1.id")) {
				questionario = new Questionario();
				questionario.setId(rs.getInt("t1.id"));
				questionario.setDescricao(rs.getString("t1.descricao"));
				questionario.setAplicacao(rs.getInt("t1.aplicacao"));
				questionario.setOrdem(rs.getInt("t1.ordem"));
				questionario.setUltimaAlteracao(new Date(rs.getDate("ultima_alteracao").getTime()));
				if (rs.getInt("t1.id_opcao_dependente") != 0) {
					Opcao opcaoDependente = new Opcao();
					opcaoDependente.setId(rs.getInt("t1.id_opcao_dependente"));
					for (Opcao opcaoDep : opcoesDependentes) {
						if (opcaoDep.getId() == opcaoDependente.getId()) {
							questionario.setOpcaoDependente(opcaoDep);
							//opcaoDep.getQuestionariosDependentes().add(questionario);
						}
					}
					if (questionario.getOpcaoDependente() == null) {
						questionario.setOpcaoDependente(opcaoDependente);
						questionariosDependentes.add(questionario);
					}
				}
				questionarios.add(questionario);
				idQuestionario = questionario.getId();
				perguntas = new ArrayList<>();
				questionario.setPerguntas(perguntas);
			}
			if (idPergunta != rs.getInt("t2.id")) {
				pergunta = new Pergunta();
				pergunta.setId(rs.getInt("t2.id"));
				pergunta.setDescricao(rs.getString("t2.descricao"));
				pergunta.setOrdem(rs.getInt("t2.ordem"));
				pergunta.setTipo(rs.getInt("t2.tipo"));
				if (rs.getInt("t2.id_opcao_dependente") != 0) {
					Opcao opcaoDependente = new Opcao();
					opcaoDependente.setId(rs.getInt("t2.id_opcao_dependente"));
					for (Opcao opcaoDep : opcoesDependentes) {
						if (opcaoDep.getId() == opcaoDependente.getId()) {
							pergunta.setOpcaoDependente(opcaoDep);
							//opcaoDep.getPerguntasDependentes().add(pergunta);
						}
					}
					if (pergunta.getOpcaoDependente() == null) {
						pergunta.setOpcaoDependente(opcaoDependente);
						perguntasDependentes.add(pergunta);
					}
				}
				idPergunta = pergunta.getId();
				switch (pergunta.getTipo()) {
				case 2:
				case 3:
				case 4:
					opcoes = new ArrayList<Opcao>();
					pergunta.setOpcoes(opcoes);
					break;
				default:
					break;
				}
				perguntas.add(pergunta);
			}
			if(pergunta.getOpcoes() != null) {
				Opcao opcao = new Opcao();
				opcao.setId(rs.getInt("t3.id"));
				opcao.setDescricao(rs.getString("t3.descricao"));
				opcao.setOrdem(rs.getInt("t3.ordem"));
				//opcao.setPerguntasDependentes(new ArrayList<Pergunta>());
				//opcao.setQuestionariosDependentes(new ArrayList<Questionario>());
				opcoes.add(opcao);
				opcoesDependentes.add(opcao);
				for (Questionario questionarioDep : questionariosDependentes) {
					if (questionarioDep.getOpcaoDependente().getId() == opcao.getId()) {
						questionarioDep.setOpcaoDependente(opcao);
						//opcao.getQuestionariosDependentes().add(questionarioDep);
					}
				}
				for (Pergunta perguntaDep : perguntasDependentes) {
					if(perguntaDep.getOpcaoDependente().getId() == opcao.getId()) {
						perguntaDep.setOpcaoDependente(opcao);
						//opcao.getPerguntasDependentes().add(perguntaDep);
					}
				}
			}
		}
		return questionarios;
	}
	
	public static void insertListRespostaFamilia(List<Questionario> questionarios, int idFamilia) throws ClassNotFoundException, SQLException {
		String sql = "insert into resposta_familia (id_familia, id_pergunta, resposta, id_opcao, selecionado) values (?,?,?,?,?)";
		int i=1;
		for (Questionario questionario : questionarios) {
			for (Pergunta pergunta : questionario.getPerguntas()) {
				if (pergunta.getOpcoes() != null) {
					for (Opcao opcao : pergunta.getOpcoes()) {
						sql+=",(?,?,?,?,?)";
					}
				}
				else
					sql+=",(?,?,?,?,?)";
				i++;
			}
		}
		sql = sql.substring(0, sql.length()-12);
		sql += " ON DUPLICATE KEY UPDATE "
				+ "resposta = VALUES(resposta), "
				+ "selecionado = VALUES(selecionado)";
		i=1;
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		for (Questionario questionario : questionarios) {
			for (Pergunta pergunta : questionario.getPerguntas()) {
				if (pergunta.getOpcoes() != null) {
					for (Opcao opcao : pergunta.getOpcoes()) {
						pstmt.setInt(i++, idFamilia);
						pstmt.setInt(i++, pergunta.getId());
						pstmt.setNull(i++, Types.VARCHAR);
						pstmt.setInt(i++, opcao.getId());
						pstmt.setBoolean(i++, opcao.isSelecionado());
					}
				}
				else {
					pstmt.setInt(i++, idFamilia);
					pstmt.setInt(i++, pergunta.getId());
					if(pergunta.getResposta() != null && !pergunta.getResposta().isEmpty())
						pstmt.setString(i++, pergunta.getResposta());
					else
						pstmt.setNull(i++, Types.VARCHAR);
					pstmt.setInt(i++, 0);
					pstmt.setBoolean(i++, false);
				}
			}
		}
		pstmt.executeUpdate();
	}
	
}
