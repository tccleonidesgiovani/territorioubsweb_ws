package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.Permissao;
import bean.Usuario;

public class PermissaoDao {
	public static List<Permissao> listarPermissoesUsuario(int idUsuario) throws ClassNotFoundException, SQLException{
		String sql = "select t1.id_permissao, t2.descricao, t1.permitido from permissao_usuario as t1 "
				+ "join permissao as t2 on t1.id_permissao = t2.id and t1.id_usuario=?";
		System.out.println(sql);
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setInt(1, idUsuario);
		ResultSet rs = pstmt.executeQuery();
		List<Permissao> permissoes = new ArrayList<Permissao>();
		while (rs.next()) {
			Permissao permissao = new Permissao();
			permissao.setId(rs.getInt("t1.id_permissao"));
			permissao.setDescricao(rs.getString("t2.descricao"));
			permissao.setPermitido(rs.getBoolean("t1.permitido"));
			permissoes.add(permissao);
		}
		return permissoes;
	}
	
	public static List<Permissao> listarPermissoesPerfil(int idPerfil) throws ClassNotFoundException, SQLException{
		String sql = "select t1.id_permissao, t2.descricao from permissao_perfil as t1 "
				+ "join permissao a t2 on t1.id_permissao = t2.id "
				+ "where t1.id_perfil = ?";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setInt(1, idPerfil);
		ResultSet rs = pstmt.executeQuery();
		List<Permissao> permissoes = new ArrayList<>();
		while (rs.next()) {
			Permissao permissao = new Permissao();
			permissao.setId(rs.getInt("t1.id"));
			permissao.setDescricao(rs.getString("t2.descricao"));
			permissoes.add(permissao);
		}
		return permissoes;
	}
	
	public static void insertByPerfilUsuario(Usuario usuario) throws ClassNotFoundException, SQLException {
		String sql = "insert into permissao_usuario (id_permissao, id_usuario, permitido) " + 
				"select id_permissao, ?, permitido from permissao_perfil_padrao where id_perfil = ? " + 
				"on duplicate key update " + 
				"permitido=values(permitido)";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setInt(1, usuario.getId());
		pstmt.setInt(2, usuario.getPerfil().getId());
		pstmt.executeUpdate();
	}
}
