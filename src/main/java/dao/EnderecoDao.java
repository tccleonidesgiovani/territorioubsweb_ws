package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import bean.Endereco;

public class EnderecoDao {
	public static void insert(Endereco endereco) throws ClassNotFoundException, SQLException {
		String sql = "insert into endereco (logradouro, numero, complemento, bairro, id_cidade, latitude, longitude) values (?, ?, ?, ?, ?, ?, ?)";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		pstmt.setString(1, endereco.getLogradouro());
		pstmt.setString(2, endereco.getNumero());
		pstmt.setString(3, endereco.getComplemento());
		pstmt.setString(4, endereco.getBairro());
		pstmt.setInt(5, endereco.getCidade().getId());
		pstmt.setDouble(6, endereco.getCoordenada().getLatitude());
		pstmt.setDouble(7, endereco.getCoordenada().getLongitude());
		pstmt.executeUpdate();
		ResultSet rs = pstmt.getGeneratedKeys();
		if (rs.next()) {
			endereco.setId(rs.getInt(1));
		}
	}
}
