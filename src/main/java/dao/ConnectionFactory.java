package dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.omg.CORBA.portable.InputStream;

public class ConnectionFactory {
	private static final int MAX_CONNETIONS = 10;
	private static ConnectionFactory daoFactory;
	private Connection connections[];
	private int cursor;
	public static ConnectionFactory getInstance() throws ClassNotFoundException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		if (daoFactory == null) {
			daoFactory = new ConnectionFactory();
			daoFactory.connections = new Connection[MAX_CONNETIONS];
			daoFactory.cursor = 0;
		}
		return daoFactory;
	}
	
	public Connection getConnection() throws SQLException {
		/*InitialContext context = new InitialContext();
		DataSource ds = (DataSource) context.lookup("java:com/env/jdbc/mysql");
		return ds.getConnection();*/

		if (connections[cursor] == null || connections[cursor].isClosed()) {
			//connections[cursor] = DriverManager.getConnection("jdbc:mysql://bd-territorio.mysql.uhserver.com/bd_territorio?useTimezone=true&serverTimezone=UTC", "ubs_user", "Uk*67sjas4Rzi@");
			connections[cursor] = DriverManager.getConnection("jdbc:mysql://localhost/bd_territorio?useTimezone=true&serverTimezone=UTC", "root", "root");
		}
		Connection connRet = connections[cursor];
		cursor=(cursor+1)%MAX_CONNETIONS;
		return connRet;
	}
}
