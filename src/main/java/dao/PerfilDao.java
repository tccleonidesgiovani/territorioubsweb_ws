package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bean.Perfil;
import bean.Usuario;

public class PerfilDao {
	public static List<Perfil> listPerfis() throws ClassNotFoundException, SQLException {
		String sql = "select id, descricao from perfil";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		List<Perfil> perfis = new ArrayList<Perfil>();
		while (rs.next()) {
			Perfil perfil = new Perfil();
			perfil.setId(rs.getInt("id"));
			perfil.setDescricao(rs.getString("descricao"));
			perfis.add(perfil);
		}
		return perfis;
	}
}
