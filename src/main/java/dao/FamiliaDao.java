package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;

import bean.Familia;

public class FamiliaDao {
	public static boolean testaFamiliaNova(Familia familia) throws ClassNotFoundException, SQLException {
		String sql = "select id from familia where id_usuario=? and id_remoto=?";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setInt(1, familia.getIdUsuario());
		stmt.setInt(2, familia.getIdLocal());
		ResultSet rs = stmt.executeQuery();
		if (rs.next()) {
			familia.setId(rs.getInt("id"));
			return false;
		}
		return true;
	}
	
	public static Date getUltimaAlteracao(int id) throws ClassNotFoundException, SQLException {
		String sql = "select ultima_alteracao from familia where id=?";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
		if(rs.next()) {
			return new Date(rs.getDate("ultima_alteracao").getTime());
		}
		return null;
	}
	
	public static void insert(Familia familia) throws ClassNotFoundException, SQLException {
		String sql= "insert into familia (id_remoto, nome, id_usuario, id_endereco, num_moradores, ultima_alteracao) values (?,?,?,?,?,?)";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		pstmt.setInt(1, familia.getIdLocal());
		pstmt.setString(2, familia.getNome());
		pstmt.setInt(3, familia.getIdUsuario());
		pstmt.setInt(4, familia.getEndereco().getId());
		pstmt.setInt(5, familia.getNumMoradores());
		pstmt.setTimestamp(6, new Timestamp(familia.getUltimaAlteracao().getTime()));
		pstmt.executeUpdate();
		ResultSet rs = pstmt.getGeneratedKeys();
		if (rs.next()) {
			familia.setId(rs.getInt(1));
		}
	}
	
}
