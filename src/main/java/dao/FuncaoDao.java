package dao;

import java.net.SocketException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import bean.Funcao;

public class FuncaoDao {
	public static List<Funcao>listFuncoes() throws SQLException, ClassNotFoundException{
		String sql = "Select id, descricao from funcao";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		ResultSet rs = pstmt.executeQuery();
		List<Funcao> funcoes = new ArrayList<Funcao>();
		while (rs.next()) {
			Funcao funcao = new Funcao();
			funcao.setId(rs.getInt("id"));
			funcao.setDescricao(rs.getString("descricao"));
			funcoes.add(funcao);
		}
		return funcoes;
	}
}
