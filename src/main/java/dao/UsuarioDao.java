package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bean.Funcao;
import bean.Perfil;
import bean.Status;
import bean.Usuario;

public class UsuarioDao {
	public static void insertUsuario(Usuario usuario) throws ClassNotFoundException, SQLException {
		String sql = "insert into usuario (nome, email, senha, id_funcao, id_status) " + "values (?, ?, ?, ?, ?)";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, usuario.getNome());
		pstmt.setString(2, usuario.getEmail());
		pstmt.setString(3, usuario.getSenha());
		pstmt.setInt(4, usuario.getFuncao().getId());
		pstmt.setInt(5, usuario.getStatus().getId());
		pstmt.executeUpdate();
	}

	public static Usuario getUsuarioByEmail(String email) throws ClassNotFoundException, SQLException {
		String sql = "select t1.id, t1.nome, t1.senha, t1.email, t1.id_funcao, t1.id_status, t1.ultimo_sinc, t1.ultima_alteracao, t2.descricao, t3.descricao, t4.id, t4.descricao "
				+ "from usuario as t1 " + "join funcao as t2 on t1.id_funcao=t2.id "
				+ "join status as t3 on t1.id_status=t3.id " + "left join perfil as t4 on t1.id_perfil=t4.id "
				+ "where t1.email=?";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, email);
		ResultSet rs = pstmt.executeQuery();
		if (rs.next()) {
			Usuario usuario = new Usuario();
			usuario.setId(rs.getInt("t1.id"));
			usuario.setNome(rs.getString("t1.nome"));
			usuario.setEmail(rs.getString("t1.email"));
			usuario.setSenha(rs.getString("t1.senha"));
			Funcao funcao = new Funcao();
			funcao.setId(rs.getInt("t1.id_funcao"));
			funcao.setDescricao(rs.getString("t2.descricao"));
			usuario.setFuncao(funcao);
			Status status = new Status();
			status.setId(rs.getInt("t1.id_status"));
			status.setDescricao(rs.getString("t3.descricao"));
			if (rs.getInt("t4.id") != 0) {
				Perfil perfil = new Perfil();
				perfil.setId(rs.getInt("t4.id"));
				perfil.setDescricao(rs.getString("t4.descricao"));
				usuario.setPerfil(perfil);
			}
			usuario.setStatus(status);
			usuario.setUltimoSinc(new Date(rs.getDate("ultimo_sinc").getTime()));
			usuario.setUltimaAlteracao(new Date(rs.getDate("ultima_alteracao").getTime()));
			return usuario;
		}
		return null;
	}

	public static void updateUsuario(Usuario usuario) throws ClassNotFoundException, SQLException {
		String sql = "update usuario set " + "nome = ? " + "email = ? " + "senha = ? " + "id_funcao = ? "
				+ "id_status = ? " + "id_funcao = ? " + "is_status = ? " + "where id = ?";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, usuario.getNome());
		pstmt.setString(2, usuario.getEmail());
		pstmt.setString(3, usuario.getSenha());
		pstmt.setInt(4, usuario.getFuncao().getId());
		pstmt.setInt(5, usuario.getStatus().getId());
		pstmt.executeUpdate();
	}

	public static List<Usuario> listUsuarios() throws ClassNotFoundException, SQLException {
		String sql = "select t1.id, t1.nome, t1.email, t1.ultima_alteracao, t1.ultimo_sinc, t2.id, t2.descricao, t3.id, t3.descricao, t4.id, t4.descricao "
				+ "from usuario as t1 join funcao as t2 on t1.id_funcao=t2.id "
				+ "join status as t3 on t1.id_status=t3.id left join perfil as t4 on t1.id_perfil=t4.id "
				+ "where t1.id_status not in (?,?)";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstm = conn.prepareStatement(sql);
		pstm.setInt(1, Status.NAO_AUTORIZADO);
		pstm.setInt(2, Status.EXCLUIDO);
		ResultSet rs = pstm.executeQuery();
		List<Usuario> usuarios = new ArrayList<Usuario>();
		while (rs.next()) {
			Usuario usuario = new Usuario();
			usuario.setId(rs.getInt("t1.id"));
			usuario.setNome(rs.getString("t1.nome"));
			usuario.setEmail(rs.getString("t1.email"));
			usuario.setUltimaAlteracao(new Date(rs.getDate("t1.ultima_alteracao").getTime()));
			usuario.setUltimoSinc(new Date(rs.getDate("ultimo_sinc").getTime()));
			Funcao funcao = new Funcao();
			funcao.setId(rs.getInt("t2.id"));
			funcao.setDescricao(rs.getString("t2.descricao"));
			usuario.setFuncao(funcao);
			Status status = new Status();
			status.setId(rs.getInt("t3.id"));
			status.setDescricao(rs.getString("t3.descricao"));
			usuario.setStatus(status);
			Perfil perfil = new Perfil();
			perfil.setId(rs.getInt("t4.id"));
			perfil.setDescricao(rs.getString("t4.descricao"));
			usuario.setPerfil(perfil);
			usuarios.add(usuario);
		}
		return usuarios;
	}
	
	public static void updatePerfil(Usuario usuario) throws ClassNotFoundException, SQLException {
		String sql = "update usuario set id_perfil = ? where id = ?";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setInt(1, usuario.getId());
		pstmt.setInt(2, usuario.getPerfil().getId());
		pstmt.executeUpdate();
	}

	public static void update(Usuario usuario) throws SQLException, ClassNotFoundException {
		String sql = "update usuario set "
				+ "nome = ?, "
				+ "email = ?, "
				+ "id_perfil = ?, "
				+ "id_funcao = ?, "
				+ "id_status = ? "
				+ "where id = ?";
		Connection conn = ConnectionFactory.getInstance().getConnection();
		PreparedStatement pstmt = conn.prepareStatement(sql);
		pstmt.setString(1, usuario.getNome());
		pstmt.setString(2, usuario.getEmail());
		if(usuario.getPerfil().getId()==0) {
			pstmt.setNull(3, Types.INTEGER);
		} else {
			pstmt.setInt(3, usuario.getPerfil().getId());
		}
		pstmt.setInt(4, usuario.getFuncao().getId());
		pstmt.setInt(5, usuario.getStatus().getId());
		pstmt.setInt(6, usuario.getId());
		pstmt.executeUpdate();
	}
}
