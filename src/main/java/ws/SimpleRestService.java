package ws;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.Produces;
import javax.ws.rs.FormParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import bean.Cidade;
import bean.Coordenada;
import bean.Endereco;
import bean.Estado;
import bean.Familia;
import bean.Funcao;
import bean.Opcao;
import bean.Perfil;
import bean.Pergunta;
import bean.Questionario;
import bean.Status;
import bean.Usuario;
import facade.FamiliaFacade;
import facade.FuncaoFacade;
import facade.PerfilFacade;
import facade.QuestionarioFacade;
import facade.UsuarioFacade;

@Path("/ws")
public class SimpleRestService {
	SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss", new Locale("en", "BR"));
	@GET
	@Path("/teste")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSomething(@QueryParam("request") String request,
			@DefaultValue("1") @QueryParam("version") int version) {
		return "test";
	}

	@GET
	@Path("/listarfuncoes")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listaFuncoes() {
		Response response;
		response = new FuncaoFacade().listarFuncoes();
		return response;
	}

	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(String json) {
		Response response = null;
		JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
		String email = jsonObject.get("email").getAsString();
		String senha = jsonObject.get("senha").getAsString();
		response = new UsuarioFacade().login(email, senha);
		return response;
	}

	@POST
	@Path("/novousuario")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response novoUsuario(String json) {
		JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
		Usuario usuario = new Usuario();
		usuario.setNome(jsonObject.get("nome").getAsString());
		usuario.setEmail(jsonObject.get("email").getAsString());
		usuario.setSenha(jsonObject.get("senha").getAsString());
		Funcao funcao = new Funcao();
		funcao.setId(jsonObject.get("funcao").getAsJsonObject().get("id").getAsInt());
		usuario.setFuncao(funcao);
		Status status = new Status();
		status.setId(Status.PENDENTE);
		usuario.setStatus(status);
		Response response;
		response = new UsuarioFacade().novoUsuario(usuario);
		return response;
	}
	
	@GET
	@Path("/listarusuariosperfis")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listaUsuarios() {
		return new UsuarioFacade().listarUsuariosPerfis();
	}
	
	@GET
	@Path("/listarperfis")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listarPerfis() {
		return new PerfilFacade().listarPerfis();
	}
	
	@POST
	@Path("/atualizaperfil")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response atualizaPerfil(String json) {
		Response response = null;
		JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
		Usuario usuario = new Usuario();
		usuario.setId(jsonObject.get("id").getAsInt());
		usuario.setNome(jsonObject.get("nome").getAsString());
		usuario.setEmail(jsonObject.get("email").getAsString());
		Funcao funcao = new Funcao();
		funcao.setId(jsonObject.get("funcao").getAsJsonObject().get("id").getAsInt());
		usuario.setFuncao(funcao);
		Status status = new Status();
		status.setId(jsonObject.get("status").getAsJsonObject().get("id").getAsInt());
		status.setDescricao(jsonObject.get("status").getAsJsonObject().get("descricao").getAsString());
		usuario.setStatus(status);
		Perfil perfil = new Perfil();
		perfil.setId(jsonObject.get("perfil").getAsJsonObject().get("id").getAsInt());
		usuario.setPerfil(perfil);
		response = new UsuarioFacade().updatePerfil(usuario);
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listarquestionarios")
	public Response listarQuestionarios() {
		return new QuestionarioFacade().listarQuestionarios();
	}
	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/atualizafamilias")
	public Response atualizaFamilias(String json) {
		Response response = null;
		System.out.println(json);
		JsonObject jsonObject = JsonParser.parseString(json).getAsJsonObject();
		int idUsuario = jsonObject.get("idUsuario").getAsInt();
		JsonArray jsonArray = jsonObject.get("familias").getAsJsonArray();
		List<Familia> familias = new ArrayList<Familia>();
		for (int i=0; i<jsonArray.size(); i++) {
			JsonObject jsonObjectFamilia = jsonArray.get(i).getAsJsonObject();
			Familia familia = new Familia();
			familia.setIdUsuario(idUsuario);
			if (jsonObjectFamilia.has("id"))
				familia.setId(jsonObjectFamilia.get("id").getAsInt());
            familia.setNome(jsonObjectFamilia.get("nome").getAsString());
            if (jsonObjectFamilia.has("idLocal")){
                familia.setIdLocal(jsonObjectFamilia.get("idLocal").getAsInt());
            }
            familia.setNumMoradores(jsonObjectFamilia.get("numMoradores").getAsInt());
            try {
				familia.setUltimaAlteracao(sdf.parse(jsonObjectFamilia.get("ultimaAlteracao").getAsString()));
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
            Endereco endereco = new Endereco();
            JsonObject jsonObjectEndereco = jsonObjectFamilia.get("endereco").getAsJsonObject();
            if (jsonObjectEndereco.has("id")) {
                endereco.setId(jsonObjectEndereco.get("id").getAsInt());
            }
            endereco.setLogradouro(jsonObjectEndereco.get("logradouro").getAsString());
            endereco.setNumero(jsonObjectEndereco.get("numero").getAsString());
            endereco.setComplemento(jsonObjectEndereco.get("complemento").getAsString());
            endereco.setBairro(jsonObjectEndereco.get("bairro").getAsString());
            Cidade cidade = new Cidade();
            cidade.setId(1);
            cidade.setNome("Balsa Nova");
            Estado estado = new Estado();
            estado.setId(1);
            estado.setNome("Paran�");
            estado.setSigla("PR");
            cidade.setEstado(estado);
            endereco.setCidade(cidade);
            JsonObject jsonObjectCoordenada = jsonObjectEndereco.get("coordenada").getAsJsonObject();
            Coordenada coordenada = new Coordenada();
            coordenada.setLatitude(jsonObjectCoordenada.get("latitude").getAsDouble());
            coordenada.setLongitude(jsonObjectCoordenada.get("longitude").getAsDouble());
            endereco.setCoordenada(coordenada);
            familia.setEndereco(endereco);
            JsonArray jsonArrayQuestionarios = jsonObjectFamilia.get("questionarios").getAsJsonArray();
            List<Questionario> questionarios = new ArrayList<>();;
            familia.setQuestionarios(questionarios);
            for(int j=0; j<jsonArrayQuestionarios.size(); j++) {
            	JsonObject jsonObjectQuest = jsonArrayQuestionarios.get(i).getAsJsonObject();
                Questionario questionario = new Questionario();
                questionario.setId(jsonObjectQuest.get("id").getAsInt());
                questionarios.add(questionario);
                if (jsonObjectQuest.has("perguntas")){
                    List<Pergunta> perguntas = new ArrayList<>();
                    questionario.setPerguntas(perguntas);
                    for (int l=0; l<jsonObjectQuest.get("perguntas").getAsJsonArray().size(); l++){
                        JsonObject jsonObjectPerg = jsonObjectQuest.get("perguntas").getAsJsonArray().get(l).getAsJsonObject();
                        Pergunta pergunta = new Pergunta();
                        pergunta.setId(jsonObjectPerg.get("id").getAsInt());
                        if (jsonObjectPerg.has("resposta")){
                            pergunta.setResposta(jsonObjectPerg.get("resposta").getAsString());
                        }
                        perguntas.add(pergunta);
                        if (jsonObjectPerg.has("opcoes")){
                            List<Opcao> opcoes = new ArrayList<>();
                            pergunta.setOpcoes(opcoes);
                            for (int k=0; k<jsonObjectPerg.get("opcoes").getAsJsonArray().size(); k++){
                                JsonObject jsonObjectOp = jsonObjectPerg.get("opcoes").getAsJsonArray().get(k).getAsJsonObject();
                                Opcao opcao = new Opcao();
                                opcao.setId(jsonObjectOp.get("id").getAsInt());
                                opcao.setSelecionado(jsonObjectOp.get("selecionado").getAsBoolean());
                                opcoes.add(opcao);
                            }
                        }
                    }
                }
            }
            familias.add(familia);
		}
		
		return new FamiliaFacade().atualizaListFamilias(familias);
	}
}
