package facade;

import java.util.List;

import javax.ws.rs.core.Response;
import com.google.gson.Gson;
import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import bean.Funcao;
import dao.FuncaoDao;
import util.responsews.ResponseBuilder;

public class FuncaoFacade {
	private static int MAX_TENTATIVAS = 10;
	public static Response listarFuncoes() {
		int tentativas = 0;
		while (tentativas<MAX_TENTATIVAS) {
			try {
				List<Funcao> funcoes = FuncaoDao.listFuncoes();
				return new ResponseBuilder().getResponseOK(new Gson().toJson(funcoes));
			}catch (CommunicationsException e) {
				tentativas++;
			}catch (Exception e) {
				return new ResponseBuilder().getDefaultResponseError();
			}
		}
		return new ResponseBuilder().getDefaultResponseError();
	}
}
