package facade;

import java.util.List;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import bean.Familia;
import bean.Funcao;
import dao.EnderecoDao;
import dao.FamiliaDao;
import dao.FuncaoDao;
import dao.QuestionarioDao;
import util.responsews.ResponseBuilder;

public class FamiliaFacade {
	private static int MAX_TENTATIVAS = 10;
	public Response atualizaListFamilias(List<Familia> familias) {
		for (Familia familia : familias) {
			int tentativas = 0;
			while (tentativas < MAX_TENTATIVAS) {
				try {
					if (familia.getId() == 0 && FamiliaDao.testaFamiliaNova(familia)) {
						EnderecoDao.insert(familia.getEndereco());
						FamiliaDao.insert(familia);
						QuestionarioDao.insertListRespostaFamilia(familia.getQuestionarios(), familia.getId());
					} else {
						// compara datas de atualização
						// se data < atualiza Banco
						// se data > atualiza Registro
					}
				} catch (CommunicationsException e) {
					tentativas++;
				} catch (Exception e) {
					e.printStackTrace();
					return new ResponseBuilder().getDefaultResponseError();
				}
			}
			if (tentativas > MAX_TENTATIVAS) {
				return new ResponseBuilder().getDefaultResponseError();
			}
		}
		return new ResponseBuilder().getResponseOK(new Gson().toJson(familias));
	}
}
