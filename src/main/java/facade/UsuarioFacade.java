package facade;

import java.net.SocketException;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import bean.Perfil;
import bean.Status;
import bean.Usuario;
import dao.PerfilDao;
import dao.PermissaoDao;
import dao.UsuarioDao;
import util.message.Error;
import util.message.Success;
import util.responsews.ResponseBuilder;

public class UsuarioFacade {
	private static int MAX_TENTATIVAS = 10;

	public Response novoUsuario(Usuario usuario) {
		int tentativas = 0;
		while (tentativas < MAX_TENTATIVAS) {
			try {
				Usuario usuarioCad = UsuarioDao.getUsuarioByEmail(usuario.getEmail());
				if (usuarioCad == null) {
					UsuarioDao.insertUsuario(usuario);
					return new ResponseBuilder()
							.getResponseOK(new Gson().toJson(Success.buildMessage(Success.MESSAGE_SUCCESS_NEW_USER)));
				} else {
					if (usuarioCad.getStatus().equals(Status.NAO_AUTORIZADO)
							|| usuarioCad.getStatus().equals(Status.EXCLUIDO)) {
						usuario.setId(usuarioCad.getId());
						UsuarioDao.updateUsuario(usuario);
					} else {
						return new ResponseBuilder().getResponseConflict(new Gson().toJson(
								Error.buildMessage(Error.ERROR_EMAIL_DUPLICATE, Error.MESSAGE_ERROR_EMAIL_DUPLICATE)));
					}
				}
			} catch (CommunicationsException e) {
				tentativas++;
			} catch (Exception e) {
				return new ResponseBuilder().getDefaultResponseError();
			}
		}
		return new ResponseBuilder().getDefaultResponseError();
	}

	public Response login(String email, String senha) {
		int tentativas = 0;
		while (tentativas < MAX_TENTATIVAS) {
			try {
				Usuario usuario = UsuarioDao.getUsuarioByEmail(email);
				if (usuario == null || !usuario.getSenha().equals(senha)) {
					return new ResponseBuilder().getResponseNotFoud(new Gson()
							.toJson(Error.buildMessage(Error.ERROR_USER_AUTH, Error.MESSAGE_ERROR_USER_AUTH)));
				} else {
					usuario.setSenha(null);
					usuario.setPermissoes(PermissaoDao.listarPermissoesUsuario(usuario.getId()));
					return new ResponseBuilder().getResponseOK(new Gson().toJson(usuario));
				}
			} catch (CommunicationsException e) {
				tentativas++;
			} catch (Exception e) {
				return new ResponseBuilder().getDefaultResponseError();
			}
		}
		return new ResponseBuilder().getDefaultResponseError();
	}
	
	public Response listarUsuariosPerfis() {
		Response response;
		int tentativas = 0;
		while (tentativas<MAX_TENTATIVAS) {
			try {
				List<Usuario> usuarios = UsuarioDao.listUsuarios();
				for(Usuario usuario : usuarios) {
					usuario.setPermissoes(PermissaoDao.listarPermissoesUsuario(usuario.getId()));
				}
				List<Perfil> perfis = PerfilDao.listPerfis();
				JsonObject jsonObject = new JsonObject();
				jsonObject.add("usuarios", JsonParser.parseString(new Gson().toJson(usuarios)));
				jsonObject.add("perfis", JsonParser.parseString(new Gson().toJson(perfis)));
				return new ResponseBuilder().getResponseOK(jsonObject.toString());
			}catch (CommunicationsException e) {
				tentativas++;
			}catch (Exception e) {
				return new ResponseBuilder().getDefaultResponseError();
			}
		}
		return new ResponseBuilder().getDefaultResponseError();
	}
	
	public Response updatePerfil(Usuario usuario) {
		int tentativas = 0;
		while (tentativas<MAX_TENTATIVAS) {
			try {
				UsuarioDao.update(usuario);
				if (usuario.getPerfil().getId()!=0)
					PermissaoDao.insertByPerfilUsuario(usuario);
				return new ResponseBuilder()
						.getResponseOK(new Gson().toJson(Success.buildMessage(Success.MESSAGE_SUCCESS_UPDATE_PERFIL)));
			}catch (CommunicationsException e) {
				tentativas++;
			}catch (Exception e) {
				return new ResponseBuilder().getDefaultResponseError();
			}
		}
		return new ResponseBuilder().getDefaultResponseError();
	}
	
}
