package facade;

import java.util.List;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import bean.Perfil;
import dao.PerfilDao;
import util.responsews.ResponseBuilder;

public class PerfilFacade {
	private static int MAX_TENTATIVAS = 10;
	public Response listarPerfis() {
		int tentativas = 0;
		while (tentativas<MAX_TENTATIVAS) {
			try {
				List<Perfil> perfis = PerfilDao.listPerfis();
				return new ResponseBuilder().getResponseOK(new Gson().toJson(perfis));
			}catch (CommunicationsException e) {
				tentativas++;
			}catch (Exception e) {
				return new ResponseBuilder().getDefaultResponseError();
			}
		}
		return new ResponseBuilder().getDefaultResponseError();
	}
}
