package facade;

import java.util.List;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mysql.cj.jdbc.exceptions.CommunicationsException;

import bean.Questionario;
import dao.QuestionarioDao;
import util.responsews.ResponseBuilder;

public class QuestionarioFacade {
	private static int MAX_TENTATIVAS = 10;
	public Response listarQuestionarios() {
		int tentativas = 0;
		while (tentativas<MAX_TENTATIVAS) {
			try {
				List<Questionario> questionarios = QuestionarioDao.listaQuestionariosFull();
				return new ResponseBuilder().getResponseOK(new Gson().toJson(questionarios));
			} catch (CommunicationsException e) {
				tentativas++;
			} catch (Exception e) {
				return new ResponseBuilder().getDefaultResponseError();
			}
		}
		return new ResponseBuilder().getDefaultResponseError();
	}
}
