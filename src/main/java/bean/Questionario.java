package bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Questionario implements Serializable {
    private int id;
    private String descricao;
    private int ordem;
    private int aplicacao;
    private Date ultimaAlteracao;
    private Opcao opcaoDependente;
    private List<Pergunta> perguntas;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getOrdem() {
        return ordem;
    }

    public void setOrdem(int ordem) {
        this.ordem = ordem;
    }

    public int getAplicacao() {
        return aplicacao;
    }

    public void setAplicacao(int aplicacao) {
        this.aplicacao = aplicacao;
    }

    public Date getUltimaAlteracao() {
        return ultimaAlteracao;
    }

    public void setUltimaAlteracao(Date ultimaAlteracao) {
        this.ultimaAlteracao = ultimaAlteracao;
    }

    public Opcao getOpcaoDependente() {
        return opcaoDependente;
    }

    public void setOpcaoDependente(Opcao opcaoDependente) {
        this.opcaoDependente = opcaoDependente;
    }

    public List<Pergunta> getPerguntas() {
        return perguntas;
    }

    public void setPerguntas(List<Pergunta> perguntas) {
        this.perguntas = perguntas;
    }

}
