package bean;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Usuario implements Serializable{
	private int id;
	private String nome;
	private String email;
	private String senha;
	private List<Permissao> permissoes;
	private Funcao funcao;
	private Status status;
	private Perfil perfil;
	private Date ultimoSinc;
	private Date ultimaAlteracao;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public List<Permissao> getPermissoes() {
		return permissoes;
	}

	public void setPermissoes(List<Permissao> permissoes) {
		this.permissoes = permissoes;
	}

	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
	
	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Date getUltimoSinc() {
		return ultimoSinc;
	}

	public void setUltimoSinc(Date ultimoSinc) {
		this.ultimoSinc = ultimoSinc;
	}

	public Date getUltimaAlteracao() {
		return ultimaAlteracao;
	}

	public void setUltimaAlteracao(Date ultima_alteracao) {
		this.ultimaAlteracao = ultima_alteracao;
	}

	public Usuario() {
	}
}
