package bean;

import java.io.Serializable;

public class Status implements Serializable{
	public static final int PENDENTE = 1;
	public static final int AUTORIZADO = 2;
	public static final int NAO_AUTORIZADO = 3;
	public static final int EXCLUIDO = 4;
	
	private int id;
	private String descricao;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Status() {
	}
}
