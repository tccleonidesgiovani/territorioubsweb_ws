package bean;

import java.io.Serializable;
import java.util.List;

public class Opcao implements Serializable{
	private int id;
	private String descricao;
	private int ordem;
	private boolean selecionado;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getOrdem() {
		return ordem;
	}
	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}
	public boolean isSelecionado() {
		return selecionado;
	}
	public void setSelecionado(boolean selecionado) {
		this.selecionado = selecionado;
	}

	@Override
	public String toString() {
		return descricao;
	}
	/*public List<Questionario> getQuestionariosDependentes() {
		return questionariosDependentes;
	}
	public void setQuestionariosDependentes(List<Questionario> questionariosDependentes) {
		this.questionariosDependentes = questionariosDependentes;
	}
	public List<Pergunta> getPerguntasDependentes() {
		return perguntasDependentes;
	}
	public void setPerguntasDependentes(List<Pergunta> perguntasDependentes) {
		this.perguntasDependentes = perguntasDependentes;
	}*/
	
}
