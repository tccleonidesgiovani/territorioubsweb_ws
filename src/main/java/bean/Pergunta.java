package bean;

import java.io.Serializable;
import java.util.List;

public class Pergunta implements Serializable{
	private int id;
	private String descricao;
	private int ordem;
	private int tipo;
	private List<Opcao> opcoes;
	private Opcao opcaoDependente;
	private String resposta;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public int getOrdem() {
		return ordem;
	}
	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public Opcao getOpcaoDependente() {
		return opcaoDependente;
	}
	public void setOpcaoDependente(Opcao opcaoDependente) {
		this.opcaoDependente = opcaoDependente;
	}
	public String getResposta() {
		return resposta;
	}
	public void setResposta(String resposta) {
		this.resposta = resposta;
	}
	public List<Opcao> getOpcoes() {
		return opcoes;
	}
	public void setOpcoes(List<Opcao> opcoes) {
		this.opcoes = opcoes;
	}
		
}
